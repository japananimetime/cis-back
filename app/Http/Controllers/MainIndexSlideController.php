<?php

namespace App\Http\Controllers;

use App\Models\MainIndexSlide;
use Illuminate\Http\Request;

class MainIndexSlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $slides = MainIndexSlide::orderBy('sort', 'asc')->get();
        $translated = $slides
            ->translate($request->header('accept-language'))
        ;
        return response($translated, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MainIndexSlide  $mainIndexSlide
     * @return \Illuminate\Http\Response
     */
    public function show(MainIndexSlide $mainIndexSlide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MainIndexSlide  $mainIndexSlide
     * @return \Illuminate\Http\Response
     */
    public function edit(MainIndexSlide $mainIndexSlide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainIndexSlide  $mainIndexSlide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainIndexSlide $mainIndexSlide)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainIndexSlide  $mainIndexSlide
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainIndexSlide $mainIndexSlide)
    {
        //
    }
}
