<?php

namespace App\Http\Controllers;

use App\Models\Direction;
use Illuminate\Http\Request;

class DirectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $directions = Direction::orderBy('sort', 'asc')->get();
        $directions->each(function ($direction) {
            $direction->text = strip_tags($direction->text);
            $direction->text = strtr($direction->text, array_flip(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES)));
            $direction->text = trim($direction->text, chr(0xC2).chr(0xA0));
            $direction->text = str_replace("\x96", "-", $direction->text);
        });
        return response($directions->translate($request->header('accept-language')), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Direction $direction)
    {
        $direction->load(['services', 'recommendations']);
        $translated = $direction
            ->translate($request->header('accept-language'))
        ;
        $translated->services = $direction->services->translate($request->header('accept-language'));
        $translated->recommendations = $direction->recommendations->translate($request->header('accept-language'));
        return response($translated, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function edit(Direction $direction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Direction $direction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Direction  $direction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Direction $direction)
    {
        //
    }
}
