<?php

namespace App\Http\Controllers;

use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use TCG\Voyager\Models\Page;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page
            ::
            whereNotIn(
                'slug', [
                          'about', 'investors',
                      ]
            )
            ->where('status', 'ACTIVE')
            ->get();

        return response($pages, Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //+
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $page = Page::where('slug', $slug)
                    ->first();

        return $page->translate($request->header('accept-language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\About $about
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\About        $about
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\About $about
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }

    public function videos()
    {
        return response(
            [
                setting('site.video1'),
                setting('site.video2'),
            ], Response::HTTP_OK
        );
    }

    public function contacts()
    {
        return response(
            [
                setting('contacts.title'),
                setting('contacts.address'),
                setting('contacts.phone'),
                setting('contacts.email'),
            ], Response::HTTP_OK
        );
    }

    public function image()
    {
        return file_get_contents('https://api.cisgroup.kz/storage/' . setting('site.background'));
    }
}
