<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::orderBy('created_at', 'desc')
                    ->paginate()
        ;

        $news->getCollection()
             ->transform(
                 function ($value) use ($request) {
                     return $value->translate($request->header('accept-language'));
                 }
             )
        ;

        return response($news, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\News $news
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, News $news)
    {
        return response($news->translate($request->header('accept-language'), 200));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\News         $news
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\News $news
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
    }
}
