<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Direction extends Model
{
    use HasFactory, Translatable;

    protected $translatable = [
        'title',
        'text',
        'textBlue'
    ];

    public function recommendations()
    {
        return $this->belongsToMany(Recommendation::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class);
    }
}
