<?php

namespace App\Models;

use App\Contracts\ProjectContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Project extends Model
{
    use HasFactory, Translatable;

    protected $translatable = [
        'text',
        'name'
    ];

    public function partner()
    {
        return $this->belongsTo(Partner::class, ProjectContract::PARTNER);
    }
}
