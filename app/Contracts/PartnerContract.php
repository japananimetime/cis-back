<?php

namespace App\Contracts;

interface PartnerContract
{
    const TABLE = 'partners';
    const IMAGE = 'image';
    const NAME = 'name';
    const MOTTO = 'motto';
}
