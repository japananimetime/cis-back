<?php

namespace App\Contracts;

interface ServiceContract
{
    const TABLE = 'services';
    const IMAGE = 'image';
    const PRICE = 'price';
    const DESCRIPTION = 'description';
    const FINISH_DATE = 'finish_date';
}
