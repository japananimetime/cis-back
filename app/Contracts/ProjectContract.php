<?php

namespace App\Contracts;

interface ProjectContract
{
    const TABLE   = 'projects';
    const NAME    = 'name';
    const TEXT    = 'text';
    const IMAGES  = 'images';
    const SORT    = 'sort';
    const PARTNER = 'partner_id';
}
