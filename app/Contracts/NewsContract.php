<?php

namespace App\Contracts;

interface NewsContract
{
    const TABLE = 'news';
    const NAME = 'name';
    const IMAGE = 'image';
    const TEXT = 'text';
    const BLUE_TEXT = 'blue_text';
}
