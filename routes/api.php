<?php

use App\Http\Controllers\PagesController;
use App\Models\Feedback;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')
     ->get(
         '/user', function (Request $request) {
         return $request->user();
     }
     )
;
Route::apiResource('directions', '\App\Http\Controllers\DirectionController');
Route::apiResource('slides', '\App\Http\Controllers\MainIndexSlideController');
Route::apiResource('partners', '\App\Http\Controllers\PartnerController');
Route::apiResource('news', '\App\Http\Controllers\NewsController');
Route::apiResource('projects', '\App\Http\Controllers\ProjectController');
Route::get(
    'footer-menus', function (Request $request) {
        $menus = Menu::where('id', '<>', 1)
                     ->with('items')
                     ->get()
                     ;
        $translated = $menus->translate($request->header('accept-language'));
        foreach ($translated as $t) {
            $t->items = $menus->where('id', $t->id)->first()->items->translate($request->header('accept-language'));
        }
    return response(
        $translated, 200
    );
}
);
Route::get('/index/news/id', function () {
    return response(setting('site.index_news_id'), 200);
});
Route::get('/index/policy', function () {
    return response(setting('site.privacy_policy'), 200);
});
Route::get('pages', '\App\Http\Controllers\PagesController@index');
Route::get('pages/{slug}', '\App\Http\Controllers\PagesController@show');
Route::get('investors/videos', '\App\Http\Controllers\PagesController@videos');
Route::get('contacts', '\App\Http\Controllers\PagesController@contacts');

Route::post(
    'feedback', function (Request $request) {
    $validated = $request->validate(
        [
            'name'    => [
                'string',
                'required',
            ],
            'phone'   => [
                'string',
                'required',
            ],
            'email'   => [
                'nullable',
                'string',
            ],
            'comment' => [
                'nullable',
                'string',
            ],
            'service' => [
                'nullable',
                'string',
            ],
            'from'    => [
                'nullable',
                'string',
            ],
            'to'      => [
                'nullable',
                'string',
            ],
        ]
    );
    Feedback
        ::query()
        ->insert(
            $validated
        )
    ;
}
);

Route::get('main/background', [PagesController::class, 'image']);
