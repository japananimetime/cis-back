<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\Contracts\PartnerContract;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PartnerContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(PartnerContract::IMAGE);
            $table->string(PartnerContract::NAME);
            $table->string(PartnerContract::MOTTO);
            $table->integer('sort')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PartnerContract::TABLE);
    }
}
