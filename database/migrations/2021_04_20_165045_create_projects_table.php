<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Contracts\ProjectContract;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProjectContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(ProjectContract::NAME);
            $table->text(ProjectContract::TEXT);
            $table->jsonb(ProjectContract::IMAGES);
            $table->integer(ProjectContract::SORT)->unsigned();
            $table->foreignId(ProjectContract::PARTNER)->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(NewsContract::TABLE);
    }
}
