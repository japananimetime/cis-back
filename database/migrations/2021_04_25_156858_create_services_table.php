<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\Contracts\ServiceContract;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ServiceContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(ServiceContract::IMAGE);
            $table->integer(ServiceContract::PRICE)->unsigned()->default(0);;
            $table->string(ServiceContract::DESCRIPTION);
            $table->timestamp(ServiceContract::FINISH_DATE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ServiceContract::TABLE);
    }
}
