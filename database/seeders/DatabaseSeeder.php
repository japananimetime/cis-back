<?php

namespace Database\Seeders;

use App\Models\About;
use App\Models\Direction;
use App\Models\MainIndexSlide;
use App\Models\News;
use App\Models\Partner;
use App\Models\Project;
use App\Models\Recommendation;
use App\Models\Service;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        \App\Models\User::factory(1)->create();
        MainIndexSlide::factory(10)->create();
        Direction::factory(8)->create();
        Partner::factory(10)->create();
        Service::factory(15)->create();
        Recommendation::factory(15)->create();
        News::factory(40)->create();
        About::factory(1)->create();
        Project::factory(10)->create();
    }
}
