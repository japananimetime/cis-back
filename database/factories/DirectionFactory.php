<?php

namespace Database\Factories;

use App\Models\Direction;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class DirectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Direction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = $this->faker->image();
        $imageFile = new File($image);
        $image = Storage::disk('public')->putFile('images', $imageFile);
        unlink($imageFile);

        return [
            'sort' => Direction::count() + 1,
            'title' => $this->faker->text(20),
            'text' => $this->faker->text(250),
            'textBlue' => $this->faker->text(50),
            'color' => $this->faker->hexColor,
            'image' => $image,
        ];
    }
}
