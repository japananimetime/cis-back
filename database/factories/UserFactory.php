<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'role_id' => 1,
            'name' => 'Hikaro',
            'email' => 'admin@admin.com',
            'avatar' => "users/default.png",
            'email_verified_at' => now(),
            'password' => '$2y$10$DIcnFJpMh5Q0WLpX65SIN.1AtMU4ny9CttyenLOp8odce8661sY9K', // password
            'remember_token' => Str::random(10),
        ];
    }
}
