<?php

namespace Database\Factories;

use App\Models\MainIndexSlide;
use Illuminate\Database\Eloquent\Factories\Factory;

class MainIndexSlideFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MainIndexSlide::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(45),
            'text' => $this->faker->text(250),
        ];
    }
}
