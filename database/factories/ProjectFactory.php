<?php

namespace Database\Factories;

use App\Contracts\ProjectContract;
use App\Models\Partner;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $images = [];
        for ($i = 0; $i < 5; $i++) {
            $image     = $this->faker->image();
            $imageFile = new File($image);
            $image     = Storage::disk('public')
                                ->putFile('images', $imageFile)
            ;
            unlink($imageFile);
            $images[] = $image;
        }

        return [
            ProjectContract::NAME    => $this->faker->text(10),
            ProjectContract::TEXT    => $this->faker->text(),
            ProjectContract::IMAGES  => json_encode($images),
            ProjectContract::SORT    => 1,
            ProjectContract::PARTNER => Partner::query()->inRandomOrder()->first()->id
        ];
    }
}
