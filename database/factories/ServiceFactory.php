<?php

namespace Database\Factories;

use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = $this->faker->image();
        $imageFile = new File($image);
        $image = Storage::disk('public')->putFile('images', $imageFile);
        unlink($imageFile);

        return [
            'image' => $image,
            'price' => random_int(100, 100000),
            'description' => $this->faker->realText(),
            'finish_date' => Carbon::create(2015, 5, 28, 0, 0, 0)->addWeeks(rand(1, 52))->format('Y-m-d H:i:s')
        ];
    }
}
