<?php

namespace Database\Factories;

use App\Models\Partner;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class PartnerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Partner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = $this->faker->image(
            __DIR__, 300, 160
        );
        $imageFile = new File($image);
        $image = Storage::disk('public')->putFile('images', $imageFile);
        unlink($imageFile);

        return [
            'image' => $image,
            'name' => $this->faker->company,
            'motto' => $this->faker->text,
        ];
    }
}
