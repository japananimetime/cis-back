<?php

namespace Database\Factories;

use App\Models\News;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class NewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = News::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = $this->faker->image();
        $imageFile = new File($image);
        $image = Storage::disk('public')->putFile('images', $imageFile);
        unlink($imageFile);

        return [
            'name' => $this->faker->text(32),
            'image' => $image,
            'text' => $this->faker->realText(2000),
            'blue_text' => $this->faker->text,
        ];
    }
}
